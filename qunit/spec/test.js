/* global QUnit, APP */


QUnit.module('Testing APP Module', {
	setupOnce: function () {
	},
	setup: function () {
		// runs before each unit test
	},
	teardown: function () {
		// runs after EACH unit test in this module
	},
	teardownOnce: function () {
		// runs once after all unit tests finished (including teardown)
	}
});

QUnit.test('sum', function (assert) {
	expect(1);
	//assert.equal(4, 4, "1+3 = 4");
	assert.equal(APP.sum(1, 3), 4, "1+3 = 4");
});