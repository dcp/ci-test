var APP = (function(_self, undefined){
	"use strict";

	_self.sum = function(a, b){
		return a+b;
	};

	_self.getCommentsFor = function(path, callback){
		callback({});
	};

	_self.getFeed = function(){
		return $.get("http://cn.nytimes.com/rss/zh-hant/");
	};

	return _self;
})(APP || {});